<?php
    require 'inc/temp.php';

    function getTitle(){
        echo 'Edit Form';
    }

    function getContent(){
        require '../connection/database.php';
        $todo_id = $_GET['todo_id'];
        $qry = "SELECT * FROM todos WHERE id = $todo_id";
        $todo = mysqli_fetch_assoc(mysqli_query($conn, $qry));
?>

<div class="vcss-container edit-form container col-lg-5">
    <div class="vcss-edit">
        <div class="vcss-edit-title">
            <h4>Edit Todo</h4>
        </div>
        <form action="../controllers/process_edit.php" method="POST">
            <div>
                <input type="hidden" name="id" value="<?= $todo['id']; ?>">
                <input class="form-control" type="text" name="todo" value="<?= $todo['list'] ?>">
            </div>
            <div class="vcss-edit-btn">
                <a class="btn btn-danger" href="home.php">Back</a>
                <button class="btn btn-success" type="submit">OK</button>
            </div>
        </form>
    </div>
</div>

<?php   
    }
?>