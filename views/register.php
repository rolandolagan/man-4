<!-- Register JS -->
<script src="../../assets/scripts/register.js" defer></script>
<?php
    require 'inc/temp.php';

    function getTitle()
    {
        echo 'Register';
    }

    function getContent()
    {
?>

<div class="vcss-register container">
    <div class="vcss-reg-wrapper">
        <h1>Register</h1>
        <form action="../controllers/process_register.php" method="POST">
            <label for="f_name">First Name:</label>
            <input class="form-control" type="text" name="f_name" placeholder="Enter your first name..." id="fName">
            <span class="text-danger"></span>

            <label for="l_name">Last Name:</label>
            <input class="form-control" type="text" name="l_name" placeholder="Enter your last name..." id="lName">
            <span class="text-danger"></span>

            <label for="email">Email:</label>
            <input class="form-control" type="email" name="email" placeholder="Enter your email..." id="email">
            <span id="err-email" class="text-danger"></span>

            <label for="password">Password:</label>
            <input class="form-control" type="password" name="password" placeholder="Enter your password...
            " id="password">
            <span class="text-danger"></span>

            <button class="vcss-reg-btn btn mt-3" type="button" id="registerBtn">Submit</button>
        </form>
        <div class="bot-note">
            <h6 class="mt-4">Already registered? <a href="login.php">Sign in</a></h5>
        </div>
    </div>
</div>

<?php
    }
?>