const registerBtn = document.getElementById('registerBtn');
registerBtn.disabled = true;

const fNameInput = document.getElementById('fName');
const lNameInput = document.getElementById('lName');
const emailInput = document.getElementById('email');
const passwordInput = document.getElementById('password');

function checkIfLetters(value, element){
    const letters = /^[A-Za-z]+$/;
    if(!value.match(letters)){
        element.nextElementSibling.textContent="Numbers and Special Characters are not allowed."
    }else{
        element.nextElementSibling.textContent = "";
    }
}

function isEmpty(value,element){
    if(value === ""){
        element.nextElementSibling.textContent = "This field is required."
        return true;
    }else{
        element.nextElementSibling.textContent = "";
        return false;
    }
}

fNameInput.addEventListener('blur', function(){
    const fNameValue = fNameInput.value;
    if(!isEmpty(fNameValue, fNameInput)){
        checkIfLetters(fNameValue, fNameInput);
    }

    enableButton();
});

lNameInput.addEventListener('blur', function(){
    const lNameValue = lNameInput.value;
    if(!isEmpty(lNameValue, lNameInput)){
        checkIfLetters(lNameValue, lNameInput);
    };

    enableButton();
})

emailInput.addEventListener('blur', function(){
    const emailValue = emailInput.value;
    isEmpty(emailValue, emailInput);

    enableButton();
})

passwordInput.addEventListener('blur', function(){
    const passwordValue = passwordInput.value;
    if(!isEmpty(passwordValue, passwordInput)){
        const letters =  /^(?=.*?[0-9])(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[^\w\s]).{8,32}$/
        if(!passwordValue.match(letters)){
            passwordInput.nextElementSibling.innerHTML = "Password must have 8-32 characters, <br> Must have at least 1 uppercase letter, 1 lowercase letter, 1 special character, and a number."
        }else if(passwordValue.match(letters)){
            enableButton();
        }else{
            passwordInput.nextElementSibling.textContent = ""
        }
    }
});

function enableButton(){
    if( fNameInput.value === "" ||
    lNameInput.value === "" ||
    emailInput.value === "" ||
    passwordInput.value === ""
    ){
        registerBtn.disabled = true;
    }else{
        registerBtn.disabled = false;
    }
}


// Form Data Transfer? 
registerBtn.addEventListener('click', function(){

    let data = new FormData;

    data.append("f_name", fNameInput.value);
    data.append("l_name", lNameInput.value);
    data.append("email", emailInput.value);
    data.append("password", passwordInput.value);

    fetch('../../controllers/process_register.php', {
        method: "POST",
        body: data
    }).then(function(response){
               return response.text();
    }).then(function(response_from_fetch){
        if(response_from_fetch === "email already used"){
            // toastr['error']("Email already exists");
            document.querySelector('#err-email').textContent = "Email already exists";
        }else{
            window.location.replace("../../views/login.php");
        }
    })
})